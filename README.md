# onliner-test &middot; [![GitHub license](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/kersh1337228/onliner-test/-/blob/master/LICENSE)
GUI-Level onliner.by website functionality testing using selenium and unittest.
