from poms.base import Page
from selenium.webdriver.common.by import By
from typing import Self, Never


class FormPage(Page):
    password = '(//input[@type="password"])[1]'
    submit_button = '//button[@type="submit"]'

    def get_field_error(
            self: Self,
            field: str
    ) -> str:
        selector = f'''{self.__getattribute__(
            f'{field}_field'
        )}/../../../..//div[contains(@class, "auth-form__description_error")]'''
        self.wait_for(selector)
        return Page.element_content(
            self._driver.find_element(By.XPATH, selector))

    def get_form_error(
            self: Self
    ) -> str:
        selector = f'//div[contains(@class, "auth-form__description_error")]'
        self.wait_for(selector)
        return Page.element_content(
            self._driver.find_element(By.XPATH, selector))

    def submit(self) -> None | Never:
        self.get('submit_button').click()
