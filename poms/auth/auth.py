from poms.base import WebDriver
from poms.auth.base import FormPage


class AuthorizationPage(FormPage):
    email = '//input[@type="text"]'

    def __init__(self, driver: WebDriver):
        super().__init__(driver, 'https://profile.onliner.by/login')
