from poms.base import WebDriver
from poms.auth.base import FormPage


class RegistrationPage(FormPage):
    email = '//input[@type="email"]'
    password_repeat = '(//input[@type="password"])[2]'
    checkbox = '//span[contains(@class, "auth-checkbox")]'
    email_button = ('//a[@class="auth-button auth-button_appendant auth-button_middle auth-form__button '
                    'auth-form__button_width_full"]')

    def __init__(self, driver: WebDriver):
        super().__init__(driver, 'https://profile.onliner.by/registration')
