from poms.base import Page, WebDriver, By
from typing import Self


class MainPage(Page):
    nav_element = ('(//ul[@class="project-navigation__list project-navigation__list_secondary"])[2]//span['
                   '@class="project-navigation__sign"]')
    category_title = '//h1[@class="catalog-form__title catalog-form__title_big-alter"]'

    def __init__(
            self: Self,
            driver: WebDriver
    ):
        super().__init__(driver, 'https://www.onliner.by')

    def get_categories(
            self: Self
    ) -> list[tuple[str, str]]:
        return list(map(
            lambda element: [
                Page.element_content(element),
                element.find_element(
                    By.XPATH, '../..'
                ).get_attribute('href')
            ],
            self.get_all('nav_element')
        ))
