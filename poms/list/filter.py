from selenium.webdriver import Keys
from selenium.webdriver.common.by import By
from poms.base import Page, WebDriver
from typing import Self


class CategoryPage(Page):
    manufacturer_filter = ('(//div[@class="catalog-form__group catalog-form__group_nonadaptive '
                           'catalog-form__group_width_full" and contains(., "Производитель")])[1]'
                           '//li[@class="catalog-form__checkbox-item catalog-form__checkbox-item_condensed"]')
    manufacturer_name = './/div[@class="catalog-form__checkbox-sign"]'

    price_filter = ('//div[@class="catalog-form__group catalog-form__group_nonadaptive catalog-form__group_width_full" '
                    'and contains(., "Цена")]//input[@type="text"]')

    product = '//div[@class="catalog-form__offers-flex"]'
    product_title = ('.//div[@class="catalog-form__description catalog-form__description_primary '
                     'catalog-form__description_base-additional catalog-form__description_font-weight_semibold '
                     'catalog-form__description_condensed-other"]')
    product_price = ('.//div[contains(@class, "catalog-form__description catalog-form__description_huge-additional '
                     'catalog-form__description_font-weight_bold catalog-form__description_condensed-other")]')

    def __init__(
            self: Self,
            driver: WebDriver,
            category: str
    ):
        super().__init__(driver, f'https://catalog.onliner.by/{category}')

    def filter(
            self,
            min_price: float,
            max_price: float
    ) -> str:
        low, high = self.get_all('price_filter')

        low.send_keys(str(min_price))
        low.send_keys(Keys.ENTER)
        self.wait(1)

        high.send_keys(str(max_price))
        high.send_keys(Keys.ENTER)
        self.wait(1)

        manufacturer = self.get('manufacturer_filter')
        manufacturer.click()
        self.wait(1)

        return Page.element_content(
            manufacturer.find_element(By.XPATH, CategoryPage.manufacturer_name))
