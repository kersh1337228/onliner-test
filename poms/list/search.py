from poms.base import Page, WebDriver
from typing import Self


class CatalogPage(Page):
    search_field = '//input[@type="text"]'
    search_overlap = '//input[@type="text" and @placeholder="Поиск" and @class="search__input"]'
    product = '//div[contains(@class, "result__wrapper")]'
    product_title = '//a[@class="product__title-link"]'

    def __init__(
            self: Self,
            driver: WebDriver
    ):
        super().__init__(driver, 'https://catalog.onliner.by')

    def search(
            self: Self,
            query: str
    ):
        self.set('search_field', '1')
        search_field = self.get('search_overlap')
        search_field.clear()
        search_field.send_keys(query)
        self.wait_for(CatalogPage.product)
