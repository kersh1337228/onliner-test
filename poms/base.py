from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver, WebElement
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from typing import Self, Never
import time


class Page:
    _driver: WebDriver

    def __init__(
            self: Self,
            driver: WebDriver,
            url: str
    ):
        self._driver = driver
        self._driver.get(url)

    def refresh(self: Self) -> None:
        self._driver.refresh()

    def get(
            self: Self,
            field: str
    ) -> WebElement:
        return self._driver.find_element(
            By.XPATH, self.__getattribute__(field))

    def get_nested(
            self: Self,
            element: WebElement,
            field: str
    ) -> WebElement:
        return element.find_element(
            By.XPATH, self.__getattribute__(field))

    def get_all(
            self: Self,
            fields: str
    ) -> list[WebElement]:
        return self._driver.find_elements(
            By.XPATH, self.__getattribute__(fields))

    def get_all_nested(
            self: Self,
            element: WebElement,
            fields: str
    ) -> list[WebElement]:
        return element.find_elements(
            By.XPATH, self.__getattribute__(fields))

    def set(
            self: Self,
            field: str,
            value: str
    ) -> None:
        self.get(field).send_keys(value)

    def wait(
            self: Self,
            seconds: float
    ) -> None:
        time.sleep(seconds)

    def wait_for(
            self: Self,
            selector: str,
            timeout: float = 10
    ) -> None | Never:
        WebDriverWait(self._driver, timeout).until(
            ec.presence_of_element_located((
                By.XPATH, selector)))

    @staticmethod
    def element_content(element: WebElement) -> str:
        return element.get_attribute(
            'innerHTML').strip().lower()
