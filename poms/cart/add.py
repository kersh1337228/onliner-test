from poms.base import Page, WebDriver
from typing import Self


class ProductPage(Page):
    add_to_cart_button = ('//a[@class="button-style button-style_base-alter button-style_primary product-aside__button '
                          'product-aside__button_narrow product-aside__button_cart button-style_expletive"]')
    goto_cart_button = ('//a[@class="button-style button-style_another button-style_base-alter '
                        'product-recommended__button"]')
    product_title = '//h1[@class="catalog-masthead__title js-nav-header"]'
    cart_product_title = ('//div[@class="cart-form__offers-item cart-form__offers-item_secondary"]//'
                          'a[@class="cart-form__link cart-form__link_primary cart-form__link_base-alter"]')

    def __init__(
            self: Self,
            driver: WebDriver,
            product_url: str
    ):
        super().__init__(driver, f'https://catalog.onliner.by/{product_url}')

    def add_to_cart(self: Self):
        title = Page.element_content(self.get('product_title'))
        self.get('add_to_cart_button').click()
        self.wait_for(ProductPage.goto_cart_button)
        self.get('goto_cart_button').click()
        self.wait_for(ProductPage.cart_product_title)
        return title
