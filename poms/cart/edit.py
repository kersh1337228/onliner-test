from poms.base import Page, WebDriver
from typing import Self, Never
import re


class CartPage(Page):
    inc_button = '//a[contains(@class, "cart-form__button_increment")]'
    dec_button = '//a[contains(@class, "cart-form__button_decrement")]'
    rem_button = '//a[contains(@class, "cart-form__button_remove")]'

    price = ('//div[@class="cart-form__offers-item cart-form__offers-item_secondary"]//div['
             '@class="cart-form__description cart-form__description_base-alter '
             'cart-form__description_font-weight_semibold cart-form__description_ellipsis '
             'cart-form__description_condensed"]')
    price_regex = re.compile('([\d&nbsp;]+,\d{2})&nbsp;р.')

    product = '//div[@class="cart-form__offers-item cart-form__offers-item_secondary"]'

    def __init__(
            self: Self,
            driver: WebDriver
    ):
        super().__init__(driver, f'https://cart.onliner.by/')

    def increment(self: Self) -> None | Never:
        self.get('inc_button').click()
        self.wait(2)

    def decrement(self: Self) -> None | Never:
        self.get('dec_button').click()
        self.wait(2)

    def get_price(self: Self) -> float:
        self.wait_for(CartPage.price)
        price_html = Page.element_content(self.get('price'))
        return float(CartPage.price_regex.findall(
            price_html)[0].replace(',', '.').replace('&nbsp;', ''))
