import unittest
import re
from typing import Self, Never
from selenium import webdriver
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.common.action_chains import ActionChains
from poms.base import Page
from poms.auth.reg import RegistrationPage
from poms.auth.auth import AuthorizationPage
from poms.list.nav import MainPage
from poms.list.search import CatalogPage
from poms.list.filter import CategoryPage
from poms.cart.add import ProductPage
from poms.cart.edit import CartPage


class OnlinerTest(unittest.TestCase):
    __driver: WebDriver

    def setUp(self: Self) -> None:
        service = webdriver.ChromeService(
            executable_path='./chrome/driver/chromedriver')
        options = webdriver.ChromeOptions()
        options.binary_location = './chrome/browser/chrome'
        options.page_load_strategy = 'eager'
        options.add_argument('--start-maximized')

        self.__driver = webdriver.Chrome(options, service)

    def test_registration_success(self: Self) -> None | Never:
        reg_page = RegistrationPage(self.__driver)
        email_domain = 'b.bb'
        reg_page.set('email', f'b@{email_domain}')
        reg_page.set('password', '12345678')
        reg_page.set('password_repeat', '12345678')
        reg_page.get('checkbox').click()
        reg_page.submit()
        reg_page.wait_for(RegistrationPage.email_button)
        email_href = reg_page.get('email_button').get_attribute('href')
        self.assertIn(f'mail.{email_domain}', email_href)

    def test_authorization_fail(self: Self) -> None | Never:
        auth_page = AuthorizationPage(self.__driver)
        auth_page.set('email', '123')
        auth_page.set('password', '123')
        auth_page.submit()
        error = auth_page.get_form_error()
        self.assertEqual(
            error, 'неверный логин или пароль',
            'Signed in with wrong data')

    def test_navigation(self: Self) -> None | Never:
        main_page = MainPage(self.__driver)
        categories = main_page.get_categories()
        for nav_title, link in categories:
            self.__driver.get(link)
            category_title = Page.element_content(
                main_page.get('category_title'))
            self.assertEqual(
                nav_title, category_title,
                f'Nav element {nav_title} leads to {category_title} category')
            self.__driver.back()

    def test_search(self: Self) -> None | Never:
        catalog_page = CatalogPage(self.__driver)
        query = 'rtx 4090'
        catalog_page.search(query)
        products = catalog_page.get_all('product')
        for product in products:
            product_html = Page.element_content(product)
            self.assertIn(
                query, product_html,
                f'No query ({query}) found in product description ({product_html})')

    def test_filter(self: Self) -> None | Never:
        min_price, max_price = 1000, 1500
        main_page = MainPage(self.__driver)
        categories = main_page.get_categories()

        for _, link in categories:
            category_page = CategoryPage(self.__driver, link.split('/')[-1])
            category_page.wait_for(CategoryPage.price_filter)
            manufacturer = category_page.filter(min_price, max_price)
            category_page.wait(1)
            products = category_page.get_all('product')
            for product in products:
                title = Page.element_content(
                    category_page.get_nested(product, 'product_title'))
                self.assertIn(
                    manufacturer, title,
                    f'Title ({title}) does not contain manufacturer ({manufacturer})')
                price_html = Page.element_content(
                    category_page.get_nested(product, 'product_price'))
                price = float(re.findall('(\d{4}),\d{2}&nbsp;р.', price_html)[0])
                self.assertGreaterEqual(
                    price, min_price,
                    f'Price ({price}) is lower than minimal ({min_price})')
                self.assertLessEqual(
                    price, max_price,
                    f'Price ({price}) is higher than maximal ({max_price})')

    def test_add_cart(self: Self) -> None | Never:
        product_page = ProductPage(self.__driver, 'videocard/gigabyte/gvn4090gamingoc2')
        title = product_page.add_to_cart()
        cart_title = Page.element_content(product_page.get('cart_product_title'))
        self.assertEqual(
            title, cart_title,
            f'Product title ({title}) does not match cart product title ({cart_title})')

    def test_edit_cart(self: Self) -> None | Never:
        product_page = ProductPage(self.__driver, 'videocard/gigabyte/gvn4090gamingoc2')
        product_page.add_to_cart()
        cart_page = CartPage(self.__driver)

        price_1 = cart_page.get_price()
        cart_page.increment()

        price_2 = cart_page.get_price()
        self.assertEqual(price_2, price_1 * 2)
        cart_page.decrement()

        price_3 = cart_page.get_price()
        self.assertEqual(price_3, price_1)

        cart_page.wait(2)
        rem_button = cart_page.get('rem_button')
        remove = ActionChains(self.__driver).move_to_element(rem_button).click(rem_button)
        remove.perform()
        cart_page.refresh()
        products = cart_page.get_all('product')
        anticipated = 0
        self.assertEqual(
            len(products), anticipated,
            f'Amount of products must be {anticipated} but equals {len(products)}')

    def tearDown(self: Self) -> None:
        self.__driver.quit()


if __name__ == '__main__':
    suite = unittest.defaultTestLoader.loadTestsFromTestCase(OnlinerTest)
    unittest.TextTestRunner().run(suite)
